package com.packt.chapter1
import akka.actor.ActorSystem
/**
  * Created by user
  */
object HelloAkkaActorSystem extends App {
  val actorSystem = ActorSystem("HelloAkka")
  println(actorSystem)
}

/*
  NOTE: On creation, an ActorSystem starts three actors:
  /user - The guardian actor: All user-defined actors are created as a child of the parent actor user, that is, when you
     create your actor in the ActorSystem, it becomes the child of the user guardian actor, and this guardian actor
     supervises your actors. If the guardian actor terminates, all your actors get terminated as well.
  /system - The system guardian: In Akka, logging is also implemented using actors. This special guardian shut downs the
    logged-in actors when all normal actors have terminated. It watches the user guardian actor, and upon termination of the guardian actor, it initiates its own shutdown.
  / - The root guardian: The root guardian is the grandparent of all the so-called top-level actors, and supervises all
    the top-level actors. Its purpose is to terminate the child upon any type of exception. It sets the ActorSystem status as terminated if the guardian actor is able to terminate all the child actors successfully.
 */