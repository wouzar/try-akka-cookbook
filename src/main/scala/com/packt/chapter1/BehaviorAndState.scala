package com.packt.chapter1

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Props

object BehaviorAndState extends App {
  val actorSystem = ActorSystem("HelloAkka")

  val actor = actorSystem.actorOf(Props[SummingActor], "summingactor")
  val actor1 = actorSystem.actorOf(Props(classOf[SummingActorWithConstructor], 10), "summingactorwithconstructor")

  println(actor.path)
  println(actor1.path)

  /*
    As soon as you send the message to the actor, it receives the message, picks up an underlying Java thread from the
    thread pool, does it's work, and releases the thread. The actors never block your current thread of execution, thus,
    they are asynchronous by nature.
   */
  while (true) {
    Thread.sleep(3000)
    actor ! 1
  }

}

class SummingActor extends Actor {
  var sum = 0

  override def receive: Receive = {
    case x: Int => sum = sum + x
      println(s"my state as sum is $sum")
    case _ =>
      println(s"I don't know what are you talking about")
  }
}

class SummingActorWithConstructor(initialSum: Int) extends Actor {
  var sum = initialSum

  override def receive: Receive = {
    case x: Int => sum = sum + x
      println(s"my state as sum is $sum")
    case _ =>
      println(s"I don't know what are you talking about")
  }
}